# javascript-sparkly-div
Add moving stars (or anything else) on any div

## In this readme... ##
* [In a nutshell](#markdown-header-in-a-nutshell)
* [Dependencies](#markdown-header-dependencies)
* [Usage](#markdown-header-usage)

## In a nutshell ##
* All you really need is the js file [sparkly.js](https://bitbucket.org/virginieLGB/javascript-sparkly-div/src/a25febacbd1df6f00a0ad06c70595b840e3791ae/src/?at=master) and jquery (I may write a full JS version when I get some time)
* [Demo](https://jsfiddle.net/virginieLGB/5nxy2y8g/12/)
* Or you can see how it works in the [src](https://bitbucket.org/virginieLGB/javascript-sparkly-div/src/a25febacbd1d?at=master) folder

## Usage ##
Basic command is `new mySparkle( jquery_element , number_of_stars , options );`, with `options` being JSON-parsed CSS properties.
### Examples ###
In your html file, just add
```
<script>
  new mySparkle( $( ".mySparkleDiv" ) , 300 );
</script>
```
Or even
```
<script>
  var sparks = new mySparkle( $( ".mySparkleDiv" ) , 150 , {
    "width" : "5px",
  } ); // creates 150 stars with a 5px width, in the div(s) with the "mySparkleDiv" class 
</script>
```
If no element is specified, then it is assumed that the stars have to be added to the `body`.

Default number of stars is 500, so you could just call `new mySparkle( )`.

## Dependencies ##
This requires [jQuery](http://jquery.com/) to have been loaded first.
You can add `<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>` in the head of your HTML file.