var mySparkle = function(elem, n, opt) {
  return this.init(elem, n, opt);
};
mySparkle.prototype.star = {
  "height": "15px",
  "width": "15px",
  "background-image": " url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA8AAAAPCAQAAACR313BAAAAS0lEQVQYGZXBQRGAIABFwVeCJBIG00AbwkgRKfE8c/kz7sLBQmIlsZE4SJwkPiRuTharze50udXX5XR4e1k4uUlcJE4SO4mNxIs/PsnOKsJwHzXnAAAAAElFTkSuQmCC )",
  "position": "absolute",
  "background-size": "contain",
  "background-repeat": "no-repeat"
};
mySparkle.prototype.verify = function() {
  if (!this.elem) {
    this.elem = $("body");
  }
  if (!this.elem.is($("body")))
    this.elem.css("position", "relative");
  if (!this.stars) {
    this.stars = [];
  }
};
mySparkle.prototype.createStar = function() {
  this.verify();
  var star = $("<div class='star'></div>");

  var randTop = Math.random() * 100;
  var randLeft = Math.random() * 100;
  star.css("top", randTop + "%");
  star.css("left", randLeft + "%");

  for (var prop in this.star) {
    star.css(prop, this.star[prop]);
  }

  this.elem.append(star);
  this.stars.push(star);
};
mySparkle.prototype.createStars = function(n) {
  for (var i = 0; i < n; i++)
    this.createStar();
};
mySparkle.prototype.moveStars = function() {
  if (!this.isPaused) {
    for (var i = 0; i < this.stars.length; i++) {
      var randTop = Math.random() * 100;
      var randLeft = Math.random() * 100;
      this.stars[i].css("top", randTop + "%");
      this.stars[i].css("left", randLeft + "%");
    }
  }
  setTimeout(this.moveStars.bind(this), 300);
};
mySparkle.prototype.init = function(elem, n, opt) {
  if (typeof(elem) == 'object') {
    if (!elem.length) {
      opt = elem;
      elem = null;
    }
  }
  if (elem == null && !this.elem)
    elem = $('body');
  if (typeof(n) == 'object') {
    opt = n;
    n = null;
  }
  if ((n == null || !n) && this.stars == undefined)
    n = 500;
  else if (n == null || !n)
    n = this.stars.length;
  if (elem)
    this.elem = elem;
  this.isPaused = false; // default, but can be overridden with the options
  for (var prop in opt) {
    this.star[prop] = opt[prop];
  }
  this.stars = [];
  this.elem.children('.star').remove();

  this.createStars(n);
  this.moveStars();
};
mySparkle.prototype.pause = function() {
  this.isPaused = true;
};
mySparkle.prototype.play = function() {
  this.isPaused = false;
};
mySparkle.prototype.toggleHideStars = function() {
  this.elem.children('.star').toggle()
};

var sparks = new mySparkle($(".black"), 150);
window.setTimeout(sparks.pause.bind(sparks), 1500); //pause the first one after 1.5s
window.setTimeout(function() { // make the stars smaller
  sparks.init({
    "width": "12px",
    "height": "12px"
  });
}, 2200);
window.setTimeout(sparks.play.bind(sparks), 3000); //and play again the first one after 3s
window.setTimeout(sparks.toggleHideStars.bind(sparks), 5000); //hide 
window.setTimeout(sparks.toggleHideStars.bind(sparks), 7000); //unhide

new mySparkle($(".thing"), 300, {
  "width": "5px"
}); // this one will work normally but won't be accessible later (can't play/pause/hide...)

window.setTimeout(function() { // 500 big sparkles all over the body
  new mySparkle({
    "width": "30px",
    "height": "30px"
  });
}, 12000);
